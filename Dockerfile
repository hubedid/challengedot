FROM node:16

WORKDIR /dot

COPY package*.json ./
RUN npm i

COPY . .

CMD ["npm", "run", "dev"]
